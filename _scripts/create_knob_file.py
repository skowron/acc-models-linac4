# folder = '/afs/cern.ch/eng/acc-models/linac4/'
# branch = '2021/'
# folder += branch
folder = '../'
filename = folder + 'operation/knobs.txt'

with open(filename, 'w') as f:
	print('# MadX name, LSA name, scaling, test trim', file = f)
	print('\n# Injection bump correction', file = f)
	for i in range(4):
                print('bi{}dhz_x_mm,    BIBEAM{}/BIDHZ_POSITION_X_MM, 1.0, 1.0' .format(i+1,i+1), file = f)
                print('bi{}dhz_px_urad, BIBEAM{}/BIDHZ_ANGLE_X_URAD,  1.0, 100.'.format(i+1,i+1), file = f)
                print('bi{}dvt_y_mm,    BIBEAM{}/BIDVT_POSITION_Y_MM, 1.0, 1.0' .format(i+1,i+1), file = f)
                print('bi{}dvt_py_urad, BIBEAM{}/BIDVT_ANGLE_Y_URAD,  1.0, 100.'.format(i+1,i+1), file = f)

