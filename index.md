---
template: overrides/main.html
---

<h1> Linac4 Optics Repository </h1>

This website contains the official optics models for the CERN Linac4.

!!! note "Locations of the repository on Gitlab and EOS"
		1) A local copy of the repository on your computer can be obtained by cloning the Gitlab repository using the following syntax:

			git clone https://gitlab.cern.ch/acc-models/acc-models-linac4.git

		2) The repository is also accessible from EOS:

			/eos/project/a/acc-models/public/linac4/

Optics for 2020 is folder scenarios/nominal
- debuncher_minus100kV   -  slightly modified energy spread from the natural linac (~280 keV, instead of ~250 keV, i.e. the natural)
- debuncher_plus650kV    -  decreased energy spread (~100 keV)
- debuncher_minus750kV   -  increased energy spread (~450 keV)

Optics from 2019 is in scenarios/2019.

Plotting of the optical functions for the web page is not implemented yet.
